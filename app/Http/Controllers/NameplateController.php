<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nameplate;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class NameplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nameplates = Nameplate::latest()->paginate(10);
        return view('nameplates.index',compact('nameplates'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('nameplates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            //'name' => 'required',
            //'email' => 'required',
        ]);
        Nameplate::create($request->all());
        return redirect()->route('nameplates.index')
                        ->with('success','Nameplate created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Nameplate $nameplate)
    {
        return view('nameplates.show',compact('nameplate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Nameplate $nameplate)
    {
        return view('nameplates.edit',compact('nameplate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Nameplate $nameplate)
    {
        $this->runPython($request, $nameplate);

        request()->validate([
            //'name' => 'required',
            //'email' => 'required',
        ]);
        $nameplate->update($request->all());
        /*return redirect()->route('nameplates.index')
                        ->with('success','Nameplate updated successfully');*/
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Nameplate::destroy($id);
        return redirect()->route('nameplates.index')
                        ->with('success','Nameplate deleted successfully');
    }

    public function runPython(Request $request,Nameplate $nameplate)
    {

        // FORMULAR DATA
        $npID               = $nameplate->id;
        $npName             = $nameplate->name;
        $npRoomnumber       = $nameplate->roomnumber;
        $npFaculty          = $nameplate->faculty;
        $npNotes            = $nameplate->notes;

        /* 1. Sprechstunde */
        $npSurgery1day      = $nameplate->surgery1day;
        $npSurgery1start    = $nameplate->surgery1start;
        $npSurgery1end      = $nameplate->surgery1end;

        /* 2. Sprechstunde */
        $npSurgery2day      = $nameplate->surgery2day;
        $npSurgery2start    = $nameplate->surgery2start;
        $npSurgery2end      = $nameplate->surgery2end;

        /* 3. Sprechstunde */
        $npSurgery3day      = $nameplate->surgery3day;
        $npSurgery3start    = $nameplate->surgery3start;
        $npSurgery3end      = $nameplate->surgery3end;

        /* START Testausgabe Daten

        echo $npName;

        /* ENDE Testausgabe Daten */

        // SYNTAX DATA


        $script = "tuerschild.py /dev/tty.usbserial-142";
        $uniString = 'python ' . $script . ' "' . $npID. ' ';
        
        $facultyHasChanged = true;
        $notesHasChanged = true;
        $trueRoomNumberHasChanged = true;
        $profNameHasChanged = true;
        $surgeryHasChanged = true;
        
        
        // changed name of faculty
        if($facultyHasChanged === true){
            $commandLine = $uniString . 'fclty' . ' ' . $npRoomnumber . '"';
            $process = new Process($commandLine);
            $process->run();
            sleep(1);
        }

        // changed name of notes
        if($notesHasChanged === true){
            $commandLine = $uniString . 'ntfy' . ' ' . $npNotes . '"';
            $process = new Process($commandLine);
            $process->run();
            sleep(1);
        }

        // changed name of roomnumber
        if($trueRoomNumberHasChanged === true){
            $commandLine = $uniString . 'rm' . ' ' . $npRoomnumber . '"';
            $process = new Process($commandLine);
            $process->run();
            sleep(1);
        }

        // changed name of roomnumber
        if($profNameHasChanged === true){
            $commandLine = $uniString . 'nm' . ' ' . $npName . '"';
            $process = new Process($commandLine);
            $process->run();
            sleep(1);
        }

        /*if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
             echo "python script run Successfully";
        }*/

        $commandLine = $uniString . 'update' . '"';
        $process = new Process($commandLine);
        $process->run();

        echo $process->getOutput();
        //dd($nameplate->name);*/
    }
}
