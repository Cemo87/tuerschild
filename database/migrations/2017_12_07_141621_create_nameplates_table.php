<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNameplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nameplates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('roomnumber');
            $table->string('faculty');
            $table->string('surgery1day');
            $table->time('surgery1start');
            $table->time('surgery1end');
            $table->string('surgery2day');
            $table->time('surgery2start');
            $table->time('surgery2end');
            $table->string('surgery3day');
            $table->time('surgery3start');
            $table->time('surgery3end');
            $table->string('notes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nameplates');
    }
}
