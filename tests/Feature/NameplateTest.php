<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class NameplateTest extends TestCase
{

    use DatabaseMigrations;

    public function setUp() {
        parent::setUp();

        $this->nameplate = factory('App\Nameplate')->create();
    }

    /** @test */
    public function a_user_can_browse_nameplates() {

        $response = $this->get('/nameplates');
        $response->assertSee($this->nameplate->roomnumber);     

    }

    /** @test */
    public function a_user_can_read_single_nameplate() {
        
        $response = $this->get('/nameplates/' . $this->nameplate->id);
        $response->assertSee($this->nameplate->roomnumber);

    }
}
