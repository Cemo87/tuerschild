@extends('layouts.default')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Show Member</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('nameplates.index') }}"> Back</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>ID:</strong>
                {{ $nameplate->id}}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                {{ $nameplate->name}}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Raumnummer:</strong>
                {{ $nameplate->roomnumber}}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Fakultät:</strong>
                {{ $nameplate->faculty}}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>1. Sprechstunde:</strong>
                {{ $nameplate->surgery1day}}, {{ $nameplate->surgery1start}} bis {{ $nameplate->surgery1end}}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>2. Sprechstunde:</strong>
                {{ $nameplate->surgery2day}}, {{ $nameplate->surgery2start}} bis {{ $nameplate->surgery2end}}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>3. Sprechstunde:</strong>
                {{ $nameplate->surgery3day}}, {{ $nameplate->surgery3start}} bis {{ $nameplate->surgery3end}}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Hinweise</strong>
                {{ $nameplate->notes}}
            </div>
        </div>
    </div>
@endsection