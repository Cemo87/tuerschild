<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nameplate extends Model
{
    protected $fillable = [
        'name', 'faculty', 'roomnumber',
        'surgery1day', 'surgery1start', 'surgery1end',
        'surgery2day', 'surgery2start', 'surgery2end',
        'surgery3day', 'surgery3start', 'surgery3end',
        'notes'
    ];
}
