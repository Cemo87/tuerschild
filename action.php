<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php // wp_head(); ?>
    <title>Türschild</title>
    <link rel="stylesheet" href="dist/css/style.css">
</head>
<body>

 <!-- content area -->     
 
<div class="door-information" style="padding-top: 10em">
    <div class="container">
        <div class="col-md-6">

            <?php
             

             $script = "tuerschild.py /dev/ttyUSB0";
             $raumnummer = $_POST["raumnummer"];
	     $uniString = 'python ' . $script . ' "' . $raumnummer . ' ';
             $fakultaet = $_POST["fakultaet"];
             $sprechzeiten = $_POST["sprechzeiten"];
             $notizen = $_POST["notizen"];
$trueRoomNumber = $_POST["trueRoomNumber"];
$profName= $_POST["profName"];
$sprechzeiten1tag= $_POST["sprechzeiten1tag"];
$sprechzeiten2tag= $_POST["sprechzeiten2tag"];
$sprechzeiten3tag= $_POST["sprechzeiten3tag"];
$sprechzeiten1von= $_POST["sprechzeiten1von"];
$sprechzeiten2von= $_POST["sprechzeiten2von"];
$sprechzeiten3von= $_POST["sprechzeiten3von"];
$sprechzeiten1bis= $_POST["sprechzeiten1bis"];
$sprechzeiten2bis= $_POST["sprechzeiten2bis"];
$sprechzeiten3bis= $_POST["sprechzeiten3bis"];


$fakultaetHasChanged = True;
$notizenHasChanged = True;
$trueRoomNumberHasChanged = True;
$profNameHasChanged = True;
$sprechzeitenHasChanged = True;



             //$data = array('as', 'df', 'gh');
             //$result = shell_exec('python /path/to/myScript.py ' . escapeshellarg(json_encode($data)));
             //$resultData = json_decode($result, true);
             //var_dump($resultData);

             //var_dump($data);
	     
	     //Sende Raumnummer
if($trueRoomNumberHasChanged == True){
             $commandLineText = $uniString . 'rm' . ' ' . $trueRoomNumber . '"';
             $command = escapeshellcmd($commandLineText);
             $output = shell_exec($command);
echo 'Python Input: ';
             echo $command;
echo '</br> Python Output:';
             echo $output;
sleep(1);
}

       
	     //Sende Fakultät
if($fakultaetHasChanged == True){
             $commandLineText = $uniString . 'fclty' . ' ' . $fakultaet . '"';
             $command = escapeshellcmd($commandLineText);
             $output = shell_exec($command);
echo 'Python Input: ';
             echo $command;
echo '</br> Python Output:';
             echo $output;
sleep(1);
}

	     //Sende Name
if($profNameHasChanged == True){
             $commandLineText = $uniString . 'nm' . ' ' . $profName . '"';
             $command = escapeshellcmd($commandLineText);
             $output = shell_exec($command);
echo 'Python Input: ';
             echo $command;
echo '</br> Python Output:';
             echo $output;

sleep(1);
}

	     //Sende Hinweise
if($notizenHasChanged == True){
             $commandLineText = $uniString . 'ntfy' . ' ' . $notizen . '"';
             $command = escapeshellcmd($commandLineText);
             $output = shell_exec($command);
echo 'Python Input: ';
             echo $command;
echo '</br> Python Output:';
             echo $output;

sleep(1);
}



// Lösche alle Sprechzeiten
if($sprechzeitenHasChanged == True){
$commandLineText = $uniString . 'tm' . '"';
             $command = escapeshellcmd($commandLineText);
             $output = shell_exec($command);
echo 'Python Input: ';
             echo $command;
echo '</br> Python Output:';
             echo $output;


sleep(1);


// Sprechzeiten 1
 	$commandLineText = $uniString . 'tm' . ' ' . $sprechzeiten1tag . ' ' . $sprechzeiten1von . ' - ' . $sprechzeiten1bis . ' Uhr' . '"';
             $command = escapeshellcmd($commandLineText);
             $output = shell_exec($command);
echo 'Python Input: ';
             echo $command;
echo '</br> Python Output:';
             echo $output;


sleep(1);

// Sprechzeiten 2
 	$commandLineText = $uniString . 'tm' . ' ' . $sprechzeiten2tag . ' ' . $sprechzeiten2von . ' - ' . $sprechzeiten2bis . ' Uhr' . '"';
             $command = escapeshellcmd($commandLineText);
             $output = shell_exec($command);
echo 'Python Input: ';
             echo $command;
echo '</br> Python Output:';
             echo $output;

sleep(1);

// Sprechzeiten 3
 	$commandLineText = $uniString . 'tm' . ' ' . $sprechzeiten3tag . ' ' . $sprechzeiten3von . ' - ' . $sprechzeiten3bis . ' Uhr' . '"';
             $command = escapeshellcmd($commandLineText);
             $output = shell_exec($command);
echo 'Python Input: ';
             echo $command;
echo '</br> Python Output:';
             echo $output;



sleep(1);
}

// Display Update

$commandLineText = $uniString . 'update' . '"';
             $command = escapeshellcmd($commandLineText);
             $output = shell_exec($command);
echo 'Python Input: ';
             echo $command;
echo '</br> Python Output:';
             echo $output;





             ?> 

            <h1>Gespeichert</h1>

            <div>

                <div class="form-group">
                    <label for="">Raumnummer</label>
                    <select id="raumnummer" name="raumnummer">
                      <option value="1">Werkraum</option>
                      <option value="2" selected="selected">Klassenzimmer</option>
                      <option value="3">Pauseraum</option>
                    </select>
                    <!--input class="form-control" type="text" name="raumnummer" value="<?php echo $raumnummer; ?>" readonly-->
                </div>

                <div class="form-group">
                    <label for="">Fakultät</label>
                    <input class="form-control" type="text" name="fakultaet" value="<?php echo $fakultaet; ?>" readonly>
                </div>

                <div class="form-group">
                    <label for="">Sprechzeiten</label>
                    <input class="form-control" type="text" name="sprechzeiten" value="<?php echo $sprechzeiten; ?>" readonly>
                </div>

                <div class="form-group">
                    <label for="">Notizen</label>
                    <textarea class="form-control"  rows="3" name="notizen" cols="3" readonly><?php echo $notizen; ?></textarea>
                </div>

                <button class="btn btn-primary" onclick="history.go(-1);">Back </button>
            </div>
             

        </div>   
    </div>
</div>

</body>
<?php // wp_footer(); ?>
</html>
