<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Name:</strong>
            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
        </div>
    </div>
    
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Raumnummer:</strong>
            {!! Form::text('roomnumber', null, array('placeholder' => 'Raumnummer','class' => 'form-control')) !!}
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Fakultät:</strong>
            {!! Form::text('faculty', null, array('placeholder' => 'Fakultät','class' => 'form-control')) !!}
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">      
        <div class="row">
            <div class="col-md-2 form-group">
                <strong>1. Sprechstunde:</strong>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <strong>Tag:</strong>
                    {{ Form::select('surgery1day', [
                        'Montag' => 'Montag',
                        'Dienstag' => 'Dienstag',
                        'Mittwoch' => 'Mittwoch',
                        'Donnerstag' => 'Donnerstag',
                        'Freitag' => 'Freitag',
                        'Samstag' => 'Samstag',
                        'Sonntag' => 'Sonntag'
                        ], null, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="col-md-2 form-group">
                von {!! Form::time('surgery1start', null, array('placeholder' => '11:00','class' => 'form-control')) !!}
            </div>
            <div class="col-md-2 form-group">
                bis {!! Form::time('surgery1end', null, array('placeholder' => '11:00','class' => 'form-control')) !!}
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">      
        <div class="row">
            <div class="col-md-2 form-group">
                <strong>2. Sprechstunde:</strong>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <strong>Tag:</strong>
                    {{ Form::select('surgery2day', [
                        'Montag' => 'Montag',
                        'Dienstag' => 'Dienstag',
                        'Mittwoch' => 'Mittwoch',
                        'Donnerstag' => 'Donnerstag',
                        'Freitag' => 'Freitag',
                        'Samstag' => 'Samstag',
                        'Sonntag' => 'Sonntag'
                        ], null, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="col-md-2 form-group">
                von {!! Form::time('surgery2start', null, array('placeholder' => '11:00','class' => 'form-control')) !!}
            </div>
            <div class="col-md-2 form-group">
                bis {!! Form::time('surgery2end', null, array('placeholder' => '11:00','class' => 'form-control')) !!}
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">      
        <div class="row">
            <div class="col-md-2 form-group">
                <strong>3. Sprechstunde:</strong>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <strong>Tag:</strong>
                    {{ Form::select('surgery3day', [
                        'Montag' => 'Montag',
                        'Dienstag' => 'Dienstag',
                        'Mittwoch' => 'Mittwoch',
                        'Donnerstag' => 'Donnerstag',
                        'Freitag' => 'Freitag',
                        'Samstag' => 'Samstag',
                        'Sonntag' => 'Sonntag'
                        ], null, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="col-md-2 form-group">
                von {!! Form::time('surgery3start', null, array('placeholder' => '11:00','class' => 'form-control')) !!}
            </div>
            <div class="col-md-2 form-group">
                bis {!! Form::time('surgery3end', null, array('placeholder' => '11:00','class' => 'form-control')) !!}
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Hinweise:</strong>
            {!! Form::textarea('notes', null, array('placeholder' => 'Bitte geben Sie einen Hinweistext ein.','class' => 'form-control')) !!}
        </div>
    </div>


    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>