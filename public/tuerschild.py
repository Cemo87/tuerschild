import serial
import sys
import time
 
port = "COM1"
baud = 9600

 
#print 'Number of arguments:', len(sys.argv), 'arguments.'
#print 'Argument List:', str(sys.argv)

file = open("debug.txt","a") 
file.write("\n\n")
file.write(str(sys.argv))

if len(sys.argv) == 3:
 port = sys.argv[1]
 payload = sys.argv[2]
 
 try:
  ser = serial.Serial(port, baud, timeout=1) #open port
  ser.close()
  ser.open()
 except Exception:
  print('Cannot access port (exception)')
  file.write("Cannot access port (exception)")
  exit(1)
 if ser.isOpen():
  ser.write ('\n')
  time.sleep (1)
  print(ser.name + ' is open...')
  file.write(ser.name + ' is open...')
  ser.write(payload + '\n')
  print("Sending string: " + payload )
  file.write("Sending string: " + payload)
  ser.close()
 else:
  print("Failed to open port " + port)
  file.write("Failed to open port " + port)

else:
 print "No valid string provided. Please check for format tuerschild.py <Port> <Payload>"
 file.write("No valid string provided. Please check for format tuerschild.py <Port> <Payload>")



#file.write("Done") 
file.close() 


exit()
