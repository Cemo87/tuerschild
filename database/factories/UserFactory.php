<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Nameplate::class, function($faker) {
    return [
        'roomnumber' => $faker->ean8,
        'name' => $faker->name,
        'faculty' => $faker->sentence,
        'surgery1' => $faker->dayOfWeek,
        'surgery2' => $faker->dayOfWeek,
        'surgery3' => $faker->dayOfWeek,
        'surgery1start' => $faker->time,
        'surgery1end' => $faker->time,
        'surgery2start' => $faker->time,
        'surgery2end' => $faker->time,
        'surgery3start' => $faker->time,
        'surgery3end' => $faker->time,
        'notes' => $faker->paragraph,
    ];
});