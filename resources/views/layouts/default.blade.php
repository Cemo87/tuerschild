<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel 5.5 CRUD example</title>
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
</head>
<body>
 
<div class="container" style="padding-top: 100px;">
    @yield('content')
</div>
 
</body>
</html>