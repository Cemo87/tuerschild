@extends('layouts.default')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Türschilder</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('nameplates.create') }}"> Create New Nameplate</a>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Sprechstunde</th>
            <th width="280px">Operation</th>
        </tr>
    @foreach ($nameplates as $nameplate)
    <tr>
        <td>{{ $nameplate->id}}</td>
        <td>{{ $nameplate->name}}</td>
        <td>{{ $nameplate->sugery1start}}</td>
        <td>
            <a class="btn btn-info" href="{{ route('nameplates.show',$nameplate->id) }}">Show</a>
            <a class="btn btn-primary" href="{{ route('nameplates.edit',$nameplate->id) }}">Edit</a>
            {!! Form::open(['method' => 'DELETE','route' => ['nameplates.destroy', $nameplate->id],'style'=>'display:inline']) !!}
            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </td>
    </tr>
    @endforeach
    </table>
    {!! $nameplates->render() !!}
@endsection